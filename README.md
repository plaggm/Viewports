Viewports
=========

For CSU's Computer Graphics Class, we assembled a virtual Solar System. This is the "Viewports" part of the project.

This is an example of using viewpoints to observe different parts of the solar system.
This project only has a few planets, but shows how to do this sort of thing using OpenGL. 

This was a class where we had to learn C++ and OpenGL constructs at the same time. So this code was not the cleanest 
that I have ever created. However, the system does work, and it is nice to see 3D in OpenGL.

This project is written in Visual Stduio 2010. There are peices of code from the course book, including libraries that the author wrote. These files gave me a basic framework to write OpenGL code. 

Due to the nature of OpenGL, there are many required libararies needed to make this project compile. I used GLUT, GLEW, and the course book libraries to get everything working properly. It also uses programmable shaders, along with other Opengl 3.2 exclusive features.

Feel free to post any questions you might have on the wiki. I really enjoyed working on this school project, so I'm glad to get to share it on GitHub.

